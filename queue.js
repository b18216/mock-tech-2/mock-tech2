let collection = [];

// Write the queue functions below.
function print() {
  return collection;
}

function enqueque(val) {
  collection.push(val);
}

function dequeue() {
  collection.shift();
}
function front() {
  return collection[0];
}
function size() {
  return collection.length;
}
function isEmpty() {
  return collection.length === 0;
}

print();
enqueque("John");
enqueque("Jane");
dequeue();
enqueque("Bob");
enqueque("Cherry");
front();
size(collection);
isEmpty();

module.exports = { collection, print, enqueque, dequeue, front, size, isEmpty };
